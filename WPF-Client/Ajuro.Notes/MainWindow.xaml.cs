﻿using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace MemoDrops
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		/// <summary>
		/// Where all notes are stored?
		/// </summary>
		string BasePath = @"C:\\Work\\Resources\\";

		/// <summary>
		/// Last selected note item.
		/// </summary>
		MyItem CurrentItem { get; set; }

		/// <summary>
		/// Use a increment to generate unique file names. Check file existence for while available.
		/// </summary>
		static int FileNr = 0;

		/// <summary>
		/// All notes as a list fo binding.
		/// </summary>
		public ObservableCollection<MyItem> FileItems { get; set; }

		/// <summary>
		/// Default public constructor
		/// </summary>
		public MainWindow()
		{
			InitializeComponent();
			CustomInitialize();
		}

		/// <summary>
		/// All custom window initialization maintained by developers.
		/// </summary>
		private void CustomInitialize()
		{
			// Don't be invasive, ask user for permission to ctreate stuffs on his disk if is not in your app folder.
			if(!Directory.Exists(BasePath))
			{
				var s = MessageBox.Show("Folder is expected. Create [" + BasePath + "] ?", "Startup", MessageBoxButton.YesNo);
				if (s == MessageBoxResult.Yes)
				{
					Directory.CreateDirectory(BasePath);
				}
				else
				{
					return;
				}
			}

			// Colect notes from disk
			var files = Directory.GetFiles(BasePath).ToList();
			ObservableCollection<MyItem> items = new ObservableCollection<MyItem>();
			foreach(string filePath in files)
			{
				items.Add(new MyItem()
				{
					Name = filePath.Substring(filePath.LastIndexOf('\\') + 1),
					NameOriginal = filePath.Substring(filePath.LastIndexOf('\\') + 1),
					Label = string.Empty
				});
			}
			FileItems = items; // Dows it improve performance to wait for the list to be constructed?

			// Wire-up events
			ResourceContent.PreviewKeyUp += ResourceContent_PreviewKeyUp;
			Resource_Name.PreviewKeyUp += Resource_Name_PreviewKeyUp;
			filesList.PreviewKeyUp += FilesList_PreviewKeyUp;

			// Provide me as model
			DataContext = this;
		}

		/// <summary>
		/// When selected note is changed.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilesList_PreviewKeyUp(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.N && Keyboard.Modifiers == ModifierKeys.Control)
			{
				NewItem();
			}
			if (e.Key == Key.D && Keyboard.Modifiers == ModifierKeys.Control)
			{
				DuplicateItem();
			}
			if (CurrentItem != null)
			{
				if (e.Key == Key.Delete)
				{
					DeleteItem();
				}
			}
		}

		/// <summary>
		/// Add a new note
		/// </summary>
		private void NewItem()
		{
			while(File.Exists(BasePath + "NewItem_" + FileNr + ".txt"))
			{
				FileNr++;
			}
			var newFileName = "NewItem_" + FileNr + ".txt"; // Microsoft.VisualBasic.Interaction.InputBox("Question?", "Title", "Default Text");
			FileNr++;
			// if (response == MessageBoxResult.Yes)
			{
				CurrentItem = new MyItem()
				{
					Name = newFileName,
					NameOriginal = newFileName,
					Label = "!!"
				};
				FileItems.Add(CurrentItem);
			}
		}

		/// <summary>
		/// Delete existent note
		/// </summary>
		private void DeleteItem()
		{
			var response = MessageBox.Show("Delete file [" + CurrentItem.Name + "] ?", "Question", MessageBoxButton.YesNo);
			if (response == MessageBoxResult.Yes)
			{
				File.Delete(BasePath + CurrentItem.NameOriginal);
				FileItems.Remove(CurrentItem);
			}
		}

		/// <summary>
		/// User is editing the note name. From this point the user can either save, creating a new file and deleting the old one or he can duplicate, creating a new file.
		/// </summary>
		/// <param name="sender">Control</param>
		/// <param name="e">Event</param>
		private void Resource_Name_PreviewKeyUp(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.N && Keyboard.Modifiers == ModifierKeys.Control)
			{
				NewItem();
			}
			if (CurrentItem != null)
			{
				if (e.Key == Key.S && Keyboard.Modifiers == ModifierKeys.Control)
				{
					SaveItem();
				}
				else if (e.Key == Key.D && Keyboard.Modifiers == ModifierKeys.Control)
				{
					DuplicateItem();
				}
				else if (e.Key != Key.LeftCtrl && e.Key != Key.LeftAlt && e.Key != Key.LeftShift)
				{
					CurrentItem.Name = Resource_Name.Text;
					if (CurrentItem.Name != CurrentItem.NameOriginal)
					{
						if (CurrentItem.Label.IndexOf("?") < 0)
						{
							CurrentItem.Label = CurrentItem.Label += "?";
						}
					}
					else
					{
						CurrentItem.Label = CurrentItem.Label.Replace("?", "");
					}
				}
			}
		}

		/// <summary>
		/// When user edits the content of the note.
		/// </summary>
		/// <param name="sender">Control</param>
		/// <param name="e">Event</param>
		private void ResourceContent_PreviewKeyUp(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.N && Keyboard.Modifiers == ModifierKeys.Control)
			{
				NewItem();
			}
			if (CurrentItem != null)
			{
				if (e.Key == Key.U && Keyboard.Modifiers == ModifierKeys.Control)
				{
					UploadItem();
				}
				else if (e.Key == Key.S && Keyboard.Modifiers == ModifierKeys.Control)
					{
						SaveItem();
					}
					else if (e.Key == Key.D && Keyboard.Modifiers == ModifierKeys.Control)
				{
					DuplicateItem();
				}
				else if (e.Key != Key.LeftCtrl && e.Key != Key.LeftAlt && e.Key != Key.LeftShift)
				{
					CurrentItem.Label = "*";
				}
			}
		}

		private void UploadItem()
		{
			var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://ajuro.azurewebsites.net/api/notes/insert");
			httpWebRequest.ContentType = "application/json";
			httpWebRequest.Method = "POST";

			using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
			{
				TextRange range = new TextRange(ResourceContent.Document.ContentStart, ResourceContent.Document.ContentEnd);
				NoteEntity item = new NoteEntity()
				{
					Author = 1,
					Content = range.Text,
					Title = CurrentItem.Name
				};

				streamWriter.Write(JsonConvert.SerializeObject(item));
				streamWriter.Flush();
				streamWriter.Close();
				StatusBartextBlock.Text = DateTime.Now.ToShortTimeString() + ": Uploaded [" + CurrentItem.Name + "]";
			}

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
			{
				var result = streamReader.ReadToEnd();
			}
		}

		/// <summary>
		/// Creating a duplicate is actually saving the work in progress as a new note.
		/// </summary>
		private void DuplicateItem()
		{
			{
				MyItem NewCurrentItem = new MyItem()
				{
					Name = CurrentItem.NameOriginal,
					NameOriginal = CurrentItem.NameOriginal,
					Label = ""
				};
				FileItems.Add(NewCurrentItem);

				if(CurrentItem.Name == CurrentItem.NameOriginal)
				{
					while(File.Exists(BasePath + CurrentItem.Name + " Copy_"+ FileNr))
					{
						FileNr++;
					}
					CurrentItem.Name += " Copy_" + FileNr;
					FileNr++;
				}
				CurrentItem.NameOriginal = CurrentItem.Name;
				TextRange range = new TextRange(ResourceContent.Document.ContentStart, ResourceContent.Document.ContentEnd);
				File.WriteAllText(BasePath + CurrentItem.Name, range.Text);
				CurrentItem.Label = "";
			}
		}

		/// <summary>
		/// Work in progress gets saved in the same file if the note name has not changes. If the name changed, a new file is created and the old file is deleted.
		/// </summary>
		private void SaveItem()
		{
			TextRange range = new TextRange(ResourceContent.Document.ContentStart, ResourceContent.Document.ContentEnd);
			File.WriteAllText(BasePath + Resource_Name.Text, range.Text);
			CurrentItem.Label = "";
			CurrentItem.Name = Resource_Name.Text;
			if (CurrentItem.Name.ToLower() != CurrentItem.NameOriginal.ToLower())
			{
				if (File.Exists(BasePath + Resource_Name.Text))
				{
					File.Delete(BasePath + CurrentItem.NameOriginal);
				}
			}
		}

		/// <summary>
		/// When another note is selected in the list.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			CurrentItem = ((MyItem)filesList.SelectedItem);
			if (CurrentItem != null)
			{
				Resource_Name.Text = CurrentItem.Name;
				FlowDocument mcFlowDoc = new FlowDocument();
				Paragraph para = new Paragraph();
				if(!File.Exists(BasePath + CurrentItem.Name))
				{
					CurrentItem.Label = "!";
					return;
				}
				para.Inlines.Add(new Run(File.ReadAllText(BasePath + CurrentItem.Name)));
				mcFlowDoc.Blocks.Add(para);
				ResourceContent.Document = mcFlowDoc;
			}
		}
	}

	/// <summary>
	/// The structure of a note
	/// </summary>
	public class MyItem: INotifyPropertyChanged
	{
		private string name { get; set; }
		/// <summary>
		/// Name of the note, is also the name of the file storing the content of the note.
		/// </summary>
		public string Name
		{
			get { return name; }
			set
			{
				name = value;
				NotifyPropertyChanged();
			}
		}

		private string nameOriginal { get; set; }
		/// <summary>
		/// The original name os stored and used when duplicates are created (to recreate a visual item in the list).
		/// </summary>
		public string NameOriginal
		{
			get { return nameOriginal; }
			set
			{
				nameOriginal = value;
				NotifyPropertyChanged();
			}
		}

		private string label { get; set; }
		/// <summary>
		/// Used as dirty flag for the note's name or content. Is emptied on save.
		/// </summary>
		public string Label
		{
			get { return label; }
			set {
				label = value;
				NotifyPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

	}
	
	public class NoteEntity
	{
		public string RowKey { get; set; }
		public int Author { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }
	}
}
